#Exercise Generic Stack

## Part 1: LimitedStack
- Make the class generic, so it can be used for every type. You should change the class header.
- Make the push method generic and implement: add the new element to the stack, but check the capacity first and maybe throw a FullStackException
- Make the pop method generic and implement: remove the most recent element, but test first if you need to throw an EmptyStackException.
- Make the top method generic and implement: return the most recent element, maybe throw an EmptyStackException.
- Test by running the TestcodeRunner...

## Part 2: LimitedNumericStack
- Create a class LimitedNumericStack that inherits from LimitedStack. Watch out: this class only accepts Number elements or subtypes of Number.
- Create both constructors
- Implement push, pop and top, but call the superclass
- The capacity, size and toString are inherited
- Test with the TestcodeRunner...

## Part 3: MyNumericStack
- Create a class MyNumericStack just like the LimitedNumericStack, but this time you do not inherit from LimitedStack. You will use an attribute named delegate of type LimitedStack.
- The class should work for all subtypes of Number
- Implement the same methods as LimitedStack and delegate the methods to the LimitedStack.
- Replace the LimitedNumericStack in the testcode by this class and test again!