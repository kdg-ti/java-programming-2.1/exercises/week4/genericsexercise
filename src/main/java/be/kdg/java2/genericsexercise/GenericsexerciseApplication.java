package be.kdg.java2.genericsexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericsexerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericsexerciseApplication.class, args);
    }

}
