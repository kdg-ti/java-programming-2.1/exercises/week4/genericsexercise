package be.kdg.java2.genericsexercise;

import java.util.ArrayList;
import java.util.List;

public class LimitedStack {
    private final int capacity;
    private int count;
    private List<Object> elements;

    public LimitedStack() {
        this(10); // default stack size
    }

    public LimitedStack(int size) {
        this.capacity = size > 0 ? size : 10;
        count = 0; // stack is aanvankelijk leeg
        elements = new ArrayList<>(size);
    }

    public void push(Object nieuw) {
        //TODO
    }

    public Object pop() {
        //TODO
        return null;
    }

    public Object top() {
        //TODO
        return null;
    }

    public int capacity() {
        return capacity;
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < capacity; i++) {
            if (i < count) {
                sb.append(elements.get(i).toString());
            } else {
                sb.append("?");
            }
            if (i < capacity - 1) {
                sb.append(", ");
            } else {
                sb.append("]");
            }
        }
        return sb.toString();
    }
}
